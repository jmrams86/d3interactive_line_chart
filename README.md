# About

Code for a basic multi-series interactive line chart using d3.js (v4).

I'm still learning d3, so this is built from various bits and pieces including structure from the three body problem animation I made. One good blog for d3 tutorials is https://datawanderings.com/
