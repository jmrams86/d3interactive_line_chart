// Define a date handling function
var parseDate = d3.timeParse("%Y-%m-%d");

// import the csv dataset and run code inside of this
d3.csv("data.csv")

    // parse the data in proper datatypes
    .row(function(d){ return {
      Name:String(d.Name),
      Date:parseDate(d.Date),
      Price_index:Number(d.Price_index)};})

    // Now work with the data
    .get(function(error,data){

        // Set the plot dimensions
        var height = (window.innerHeight||document.documentElement.clientHeight||document.body.clientHeight)/2;
        var width = (window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth)/1.5;

        // Get axis scale maxes and mins
        var max = d3.max(data, function(d){ return d.Price_index; });
        var min = d3.min(data, function(d){ return d.Price_index; });
        var minDate = d3.min(data, function(d) { return d.Date; });
        var maxDate = d3.max(data, function(d) { return d.Date; });

        var y = d3.scaleLinear()
                  .domain([min*0.9,max])
                  .range([height,0]);
        var x = d3.scaleTime()
                  .domain([minDate,maxDate])
                  .range([0,width]);

        var yAxis = d3.axisLeft(y);
        var xAxis = d3.axisBottom(x)
                      .ticks(d3.timeMonth.every(2))
                      .tickFormat(d3.timeFormat('%b %y'));

        // Set up the tooltip
        var tooltip = d3.select("div#container").append("div").style("opacity","0").style("position","absolute");

        // Initialise the svg canvas
        var svg =  d3.select("div#container").append("svg")
                        .attr("preserveAspectRatio", "xMinYMin meet")
                        .attr("height",height*1.5)
                        .attr("width",width*1.2)
                        .classed("svg-content", true);

        var margin = {left:50,right:50,top:40,bottom:9};


        // Create chart group
        var chartGroup = svg.append("g")
                            .attr("transform","translate("+margin.left+","+margin.top+")");

        // Set up line generator
        var line = d3.line()
                     .x(function(d){ return x(d.Date); })
                     .y(function(d){ return y(d.Price_index); });

        // Group the data by fund/index
        var dataGroup = d3.nest()
                          .key(function(d){ return d.Name; })
                          .entries(data);

        //  Predefine styles and actions
        var colourDict = {"Market Index":"#25245d",
                          "Investment A":"#e64e46",
                          "Investment B":"#b97b30"}

        var stroke = 1 // Set line default stroke width

        // Setup hover highlight functions
        var highlightOnHover = function() { d3.select(this).style("stroke-width", stroke*2);
                                            d3.select(document.getElementById("label_" + this.id)).style("font-weight", "bold");}
        var highlightOff = function() { d3.select(this).style("stroke-width", stroke);
                                        d3.select(document.getElementById("label_" + this.id)).style("font-weight", "normal");}

        // Add relative line for 100 index
       chartGroup.append("line")
            .attr("class","relative-line")
           .attr("x1", x(minDate))
           .attr("y1", y(100))
           .attr("x2", x(maxDate))
           .attr("y2", y(100));


         // Add lines to the chart
         chartGroup.selectAll("path")
                   .data(dataGroup)
                   .enter()
                   .append("path")
                   .attr("d", function(d) { return line(d.values); })
                   .attr('id', function(d) { return d.key; })
                   .attr('stroke', function(d) { return colourDict[d.key]; })
                   .attr("stroke-width", stroke)
                   .attr("fill", "none")
                   .on("mouseover",highlightOnHover)
                   .on("mouseout", highlightOff);

         // Append x and y axis
         chartGroup.append("g").attr("class","x axis").attr("transform","translate(0,"+height+")").call(xAxis);
         chartGroup.append("g").attr("class","y axis").call(yAxis)
                                                      .append("text")
                                                      .attr("class", "axis-title")
                                                      .attr("transform", "rotate(-90)")
                                                      .attr("y", 6)
                                                      .attr("dy", ".71em")
                                                      .style("text-anchor", "end")
                                                      .attr("fill", "black")
                                                      .text("Index  (100)");

         // Append line labels at line ends with value
        chartGroup.append("g")
                   .selectAll("path")
                   .data(dataGroup)
                   .enter()
                   .append('text')
                   .attr('id', function(d) { return "label_" + d.key; })
                   .attr('x', x(maxDate)+2)
                   .attr('y', function(d) { return y(d.values[d.values.length-1].Price_index); })
                   .text(function(d, i) {
                     var name = d.key;
                     var value = d.values[d.values.length-1].Price_index.toFixed(1);
                     return name + " (" + value + ")";
                   })
                   .style('fill', function(d) { return colourDict[d.key]; });


       });
